export const environment = {
  production: true,
  apiUrl: 'https://algamoney-api-rafael.herokuapp.com',

  tokenWhitelistedDomains: [ new RegExp('algamoney-api-rafael.herokuapp.com') ],
  tokenBlacklistedRoutes: [ new RegExp('\/oauth\/token') ]
};
